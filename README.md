# 网络聊天室-C
## Getting started
* 本项目在QT开发&运行
* 本项目使用C/C++开发

## 项目介绍
网络聊天室的通讯的实现是基于TCP协议下实现的多人聊天室。
其UI界面是基于QT原始画风。

## 文件大纲
* main.cpp ->网络聊天室的登录入口
* client.cpp ->客户端
* server。cpp ->服务端

